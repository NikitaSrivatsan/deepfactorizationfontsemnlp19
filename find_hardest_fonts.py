from arguments import parse_args
import pickle

from arguments import parse_args
from dataio import load_corpus
from enums import *
from models.font_vae import FontVAE
from models.nearest_neighbor import NearestNeighbor
from preprocessor import Preprocessor
from set_paths import paths

def find_hardest(model, corpus, name):
    metric = model.hyperparams['metric']
    print "Finding hardest fonts for NN based on", metric
    reverse = metric == Metric.mse
    sorted_corpus = sorted(corpus, key=lambda x: model.evaluate([x], metric), reverse=reverse)
    hardest_10 = [filename for filename,font in sorted_corpus[:len(sorted_corpus)/10]]
    outfile = open("hardest_10_{}.txt".format(name), 'w')
    for filename in hardest_10:
        outfile.write(filename)
        outfile.write('\n')
    outfile.close()

def main(config):
    config.tgt_epoch = 0
    config.model = Model.nearest_neighbor
    config.blanks = 0
    print config
    # set data types
    if config.gpu:
        config.float = torch.cuda.FloatTensor
        config.long = torch.cuda.LongTensor
    else:
        config.float = torch.FloatTensor
        config.long = torch.LongTensor
    # load data
    if config.mode != Mode.test:
        print "Recycling dev data for test"
    train, dev, test = load_corpus(config)
    print "Train images:", len(train)
    print "Dev images:", len(dev)
    print "Test images:", len(test)
    dataset = paths['cap64']
    test_dict = pickle.load(open(dataset + "/test_dict/dict.pkl", 'rb'))
    config.test_dict = test_dict
    proc = Preprocessor(config, train)
    config.proc = proc
    model = NearestNeighbor(config)
    # feed in dev twice as a dumb hack
    # because it wants dev_hard even though it doesn't exist
    model.train(train, dev, dev, test)
    # see which fonts were the hardest
    if config.mode == Mode.test:
        find_hardest(model, test, "test")
    else:
        find_hardest(model, dev, "dev")

if __name__ == "__main__":
    config = parse_args()
    main(config)
