import pickle

from imageio import imwrite
import numpy as np
from scipy.spatial.distance import cosine

from arguments import parse_args
from dataio import load_corpus
from enums import *
from models.font_vae import FontVAE
from models.nearest_neighbor import NearestNeighbor
from preprocessor import Preprocessor
from set_paths import paths

from utils import prep_write

def main(config):
    print config
    # set data types
    if config.gpu:
        config.float = torch.cuda.FloatTensor
        config.long = torch.cuda.LongTensor
    else:
        config.float = torch.FloatTensor
        config.long = torch.LongTensor
    # load data
    if config.mode != Mode.test:
        print "Recycling dev data for test"
    train, dev, _, test, _ = load_corpus(config)
    # filter held out
    print "Train images:", len(train)
    print "Dev images:", len(dev)
    print "Test images:", len(test)
    dataset = paths['cap64']
    test_dict = pickle.load(open(dataset + "/test_dict/dict.pkl", 'rb'))
    config.test_dict = test_dict
    proc = Preprocessor(config, train)
    config.proc = proc
    # invert data
    train_lookup = {}
    dev_lookup = {}
    test_lookup = {}
    for filename,datum in train:
        train_lookup[filename] = datum
    for filename,datum in dev:
        dev_lookup[filename] = datum
    for filename,datum in test:
        test_lookup[filename] = datum
    # create model
    if config.model == Model.font_vae:
        model = FontVAE(config)
    elif config.model == Model.nearest_neighbor:
        model = NearestNeighbor(config)
    # get the embedding for fonts
    '''
    filename1 = "FiraSans-UltraLight.0.0.png"
    filename2 = "FiraSans-Bold.0.0.png"
    filename1 = "Plump.0.0.png"
    filename2 = "XRAYTEDSKEW.0.0.png"
    filename1 = "OdalisqueNF.0.0.png"
    filename2 = "PetitFormalScript-Regular.0.0.png"
    filename1 = "SourceSerifPro-Semibold.0.0.png"
    filename2 = "STRSLOUI.0.0.png"
    filename1 = "AccanthisADFStd-Bold.0.0.png"
    filename2 = "AccanthisADFStd-BoldItalic.0.0.png"
    filename1 = "Amethysta-Regular.0.0.png"
    filename2 = "Chewy.0.0.png"
    filename1 = "BurgerDoodleTwoNF.0.0.png"
    filename2 = "CAT North Shadow.0.0.png"
    '''
    filename1 = "Courier Prime Bold.0.0.png"
    filename2 = "Courier Prime Sans Italic.0.0.png"
    datum1 = train_lookup[filename1]
    datum2 = train_lookup[filename2]
    emb1 = model.encoder.forward(config.float(datum1), model.y)[0]
    emb2 = model.encoder.forward(config.float(datum2), model.y)[0]
    # interpolate between them
    output = np.zeros([26 * 64, 64 * 13], dtype=np.uint8)
    output[:, 0:64] = prep_write(config.float(datum1) / 255.)
    output[:, (12*64):(13*64)] = prep_write(config.float(datum2) / 255.)
    for i in range(11):
        lamb = i / 10.
        inter_emb = emb1 * (1 - lamb) + emb2 * lamb
        recon = model.decoder.forward(inter_emb, model.y)
        prep_recon = prep_write(recon)
        output[:, ((i+1)*64):((i+2)*64)] = prep_recon
    imwrite("{}/{}/{}/interpolation_{}_{}.png".format(paths['images'], config.mode, config.model, filename1.split('.png')[0], filename2.split('.png')[0]), output)
    # do some arithmetic
    filename3 = "Charnarr.0.0.png"
    datum3 = train_lookup[filename3]
    emb3 = model.encoder.forward(config.float(datum3), model.y)[0]
    output = np.zeros([26 * 64, 64 * 14], dtype=np.uint8)
    output[:, 0:64] = prep_write(config.float(datum3) / 255.)
    for i in range(11):
        lamb = i / 10.
        analogy = emb3 + (emb2 - emb1) * lamb
        recon = model.decoder.forward(analogy, model.y)
        prep_recon = prep_write(recon)
        output[:, ((i+1)*64):((i+2)*64)] = prep_recon
    nearest_cos_datum = None
    nearest_cos = 0.0
    for datum in train_lookup.values():
        candidate_emb = model.encoder.forward(config.float(datum), model.y)[0]
        candidate_cos = cosine(candidate_emb.detach().cpu().numpy(), analogy.detach().cpu().numpy())
        if candidate_cos > nearest_cos:
            nearest_cos = candidate_cos
            nearest_cos_datum = datum
    output[:, (12*64):(13*64)] = prep_write(config.float(nearest_cos_datum) / 255.)
    nearest_l2_datum = None
    nearest_l2 = float('inf')
    for datum in train_lookup.values():
        candidate_l2 = ((config.float(datum) - recon * 255) ** 2).sum()
        if candidate_l2 < nearest_l2:
            nearest_l2 = candidate_l2
            nearest_l2_datum = datum
    output[:, (13*64):(14*64)] = prep_write(config.float(nearest_l2_datum) / 255.)
    imwrite("{}/{}/{}/analogy_{}_{}_{}.png".format(paths['images'], config.mode, config.model, filename1.split('.png')[0], filename2.split('.png')[0], filename3.split('.png')[0]), output)
    # sweep dimensions
    for dim in range(config.z_size):
        filename4 = "Chonker.0.0.png"
        datum4 = train_lookup[filename4]
        emb4 = model.encoder.forward(config.float(datum4), model.y)[0]
        output = np.zeros([26 * 64, 64 * 11], dtype=np.uint8)
        for i in range(11):
            lamb = (i / 10.) * 4 - 2
            emb4[dim] = lamb
            recon = model.decoder.forward(emb4, model.y)
            prep_recon = prep_write(recon)
            output[:, (i*64):((i+1)*64)] = prep_recon
        imwrite("{}/{}/{}/sweep_{}_{}.png".format(paths['images'], config.mode, config.model, filename4.split('.png')[0], dim), output)

if __name__ == '__main__':
    config = parse_args()
    main(config)
