import pickle

from arguments import parse_args
from dataio import load_corpus
from enums import *
from models.font_vae import FontVAE
from models.nearest_neighbor import NearestNeighbor
from preprocessor import Preprocessor
from set_paths import paths

def load_hard(corpus, name):
    hard_font_file = open("hardest_10_{}.txt".format(name))
    hard_fonts = set()
    for line in hard_font_file:
        hard_fonts.add(line.strip())
    return [(x,y) for (x,y) in corpus if x in hard_fonts]

def main(config):
    print config
    # set data types
    if config.gpu:
        config.float = torch.cuda.FloatTensor
        config.long = torch.cuda.LongTensor
    else:
        config.float = torch.FloatTensor
        config.long = torch.LongTensor
    # load data
    if config.mode != Mode.test:
        print "Recycling dev data for test"
    train, dev, dev_hard, test, test_hard = load_corpus(config)
    '''
    # filter held out
    dev_hard = load_hard(dev, "dev")
    test_hard = load_hard(test, "test")
    '''
    print "Train images:", len(train)
    print "Dev images:", len(dev)
    print "Dev (hard) images:", len(dev_hard)
    print "Test images:", len(test)
    print "Test (hard) images:", len(test_hard)
    dataset = paths['cap64']
    test_dict = pickle.load(open(dataset + "/test_dict/dict.pkl", 'rb'))
    config.test_dict = test_dict
    proc = Preprocessor(config, train)
    config.proc = proc
    if config.model == Model.font_vae:
        model = FontVAE(config)
    elif config.model == Model.nearest_neighbor:
        model = NearestNeighbor(config)
    model.config.test_dict = test_dict
    model.train(train, dev, dev_hard, test, test_hard)

if __name__ == '__main__':
    config = parse_args()
    main(config)
