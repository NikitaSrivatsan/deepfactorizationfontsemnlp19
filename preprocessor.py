import torch

from utils import binarize

class Preprocessor:
    def __init__(self, config, corpus):
        self.config = config
        self.eps = 0.1
        self.mu = None
        self.ZCA = None
        self.collect_stats(corpus)

    def collect_stats(self, corpus):
        X = torch.zeros(len(corpus) * 26, 64 * 64).type(self.config.float)
        for i,font in enumerate(corpus):
            datum = self.config.float(font[1])
            if self.config.binarize:
                datum = binarize(datum)
            for j,letter in enumerate(datum):
                X[i * 26 + j] = letter.view(-1)
        # per pixel mean
        self.mu = torch.mean(X, dim=0)
        X -= self.mu.view(1, -1)
        # compute covariance and perform SVD on it
        cov = 1. / (X.shape[0] - 1) * torch.matmul(X.t(), X)
        U, S, V = torch.svd(cov)
        # build the ZCA transformation matrix
        self.ZCA = torch.matmul(torch.matmul(U, torch.diag(1./((S + self.eps) ** 0.5))), U.t())

    def preprocess(self, x):
        x_shape = x.shape
        x_flat = x.view(26, -1)
        x_cent = x_flat - self.mu.view(1, -1)
        zca_x = torch.matmul(self.ZCA.view(1, 64 * 64, 64 * 64), x_cent.view(26, -1, 1))
        return zca_x.view(x_shape)
