import pickle
import csv

import bhtsne
import numpy as np
from skimage.measure import block_reduce
from torch.autograd import Variable

from arguments import parse_args
from dataio import load_corpus
from enums import *
from models.font_vae import FontVAE
from models.nearest_neighbor import NearestNeighbor
from preprocessor import Preprocessor
from set_paths import paths

def main(config):
    print config
    # set data types
    if config.gpu:
        config.float = torch.cuda.FloatTensor
        config.long = torch.cuda.LongTensor
    else:
        config.float = torch.FloatTensor
        config.long = torch.LongTensor
    # load data
    train, dev, _, test, _ = load_corpus(config)
    print "Train images:", len(train)
    dataset = paths['cap64']
    test_dict = pickle.load(open(dataset + "/test_dict/dict.pkl", 'rb'))
    config.test_dict = test_dict
    proc = Preprocessor(config, train)
    config.proc = proc
    # set up model
    if config.model == Model.font_vae:
        model = FontVAE(config)
    else:
        sys.exit("Unsupported model type for TSNE")
    # format the fonts and run bhtsne
    font_matrix = []
    filenames = []
    for filename, datum in train:
        '''
        font = np.array(train[i][1]).reshape([26, 64, 64])
        font = block_reduce(font, (1,3,3), np.mean)
        font_matrix.append(font.reshape([-1]))
        '''
        font_matrix.append(model.encoder.forward(Variable(config.float(datum)), model.y)[0].detach().cpu().numpy())
        filenames.append(filename)
    font_matrix = np.array(font_matrix)
    print "Initial dimensions:", font_matrix.shape
    embed_matrix = bhtsne.run_bh_tsne(font_matrix, initial_dims=font_matrix.shape[1], perplexity=25)
    #embed_matrix = np.concatenate([np.array(filenames).reshape([-1, 1]), embed_matrix], axis=1)
    #np.savetxt("tsne/{}_train.csv".format(config.mode), embed_matrix, delimiter=',', fmt='%s', encoding='utf-8')
    writer = csv.writer(open("tsne/{}_train.csv".format(config.mode), 'w'))
    for filename,vec in zip(filenames, embed_matrix):
        writer.writerow([filename] + [str(x) for x in vec])

if __name__ == '__main__':
    config = parse_args()
    main(config)
