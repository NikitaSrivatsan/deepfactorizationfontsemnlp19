import random
import sys

from imageio import imwrite
import numpy as np
from skimage.measure import compare_ssim as ssim
import torch
from torch.nn import Parameter
from torch.autograd import Variable

from enums import *
from model import Model
from set_paths import paths
from utils import gaussian_reparam, kl_divergence, prep_write

class NearestNeighbor(Model):
    def build(self):
        # define hyperprameters
        self.curr_epoch = -1
        self.hyperparams = {'metric' : Metric.mse}
        self.fonts = {}

    def train_step(self, font):
        filename, datum = font
        datum = Variable(self.config.float(datum))
        self.fonts[filename] = datum

    def reconstruct(self, datum, mask):
        datum_hat = None
        if self.hyperparams['metric'] == Metric.mse:
            best_mse = float("inf")
            for f in self.fonts.values():
                # only consider the observed characters to find neighbor
                candidate_mse = torch.nn.functional.mse_loss((f / 255.) * mask, (datum / 255.) * mask)
                if candidate_mse < best_mse:
                    best_mse = candidate_mse
                    datum_hat = f
        elif self.hyperparams['metric'] == Metric.ssim:
            best_ssim = float("-inf")
            for f in self.fonts.values():
                candidate_ssim = 0.0
                for j in range(26):
                    if mask[j,0] == 1:
                        candidate_ssim += float(ssim(f[j].detach().cpu().numpy() / 255., datum[j].detach().cpu().numpy() / 255., data_range=(datum[j].max()-datum[j].min()).item() / 255.))
                candidate_ssim /= mask.sum().item()
                if candidate_ssim > best_ssim:
                    best_ssim = candidate_ssim
                    datum_hat = f
        return datum_hat

    def visualize_reconstructions(self, corpus, prefix, max_fonts=None):
        if max_fonts is None:
            max_fonts = len(corpus)
        for i,font in enumerate(corpus[:max_fonts]):
            filename, datum = font
            datum = Variable(self.config.float(datum))
            mask = self.get_mask(filename)
            datum_hat = self.reconstruct(datum, mask)
            # write reconstructions to file
            inter = datum * mask + datum_hat * (1 - mask)
            path = "{}/{}/{}/recon_{}_b{}_{}_{}".format(paths['images'], self.config.mode, self.config.model, prefix, self.config.blanks, i, filename)
            imwrite(path, prep_write(inter / 255.))

    def get_mask(self, filename):
        mask = Variable(torch.ones(26, 1)).type(self.config.float)
        if filename in self.config.test_dict:
            blank_ids = self.config.test_dict[filename][:self.config.blanks]
        else:
            blank_ids = range(26)
            random.shuffle(blank_ids)
            blank_ids = blank_ids[:self.config.blanks]
        mask[blank_ids] = 0
        return mask

    def sample_fonts(self):
        num_samples = 100
        for i in range(100):
            sample = prep_write(random.choice(self.fonts.values()) / 255.)
            imwrite("{}/{}/{}/sample_{}.png".format(paths['images'], self.config.mode, self.config.model, i), sample)
