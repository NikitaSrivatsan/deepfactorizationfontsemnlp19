import random
import torch
from torch.autograd import Variable

from skimage.measure import compare_ssim as ssim

from configuration import volatile
from enums import Metric
import enums
from set_paths import paths
from utils import print_bar

class Model(torch.nn.Module):
    def __init__(self, config):
        super(Model, self).__init__()
        self.config = config
        self.build()
        if config.load_path is not None:
            self.load()
            for var in volatile:
                newval = getattr(config, var)
                setattr(self.config, var, newval)

    def build(self):
        pass

    def train(self, train, dev, dev_hard, test, test_hard):
        self.training = False
        for e in range(self.curr_epoch + 1, self.config.tgt_epoch + 1):
            self.curr_epoch = e
            # train on fonts
            self.training = True
            random.shuffle(train)
            for i,font in enumerate(train):
                print_bar(i, len(train))
                self.train_step(font)
            # save and evaluate
            self.training = False
            if e % self.config.eval_every == 0 or e == self.config.tgt_epoch:
                self.print_metrics([train, dev, dev_hard], ["tr", "d", "dh"])
                self.sample_fonts()
                self.visualize_reconstructions(dev, "dev", max_fonts=100)
                self.visualize_reconstructions(dev_hard, "dev_hard", max_fonts=100)
            if self.config.save_every > 0 and e % self.config.save_every == 0 or e == self.config.tgt_epoch:
                self.save()
        self.print_metrics([train, dev, dev_hard, test, test_hard], ["tr", "d", "dh", "ts", "tsh"])
        self.sample_fonts()
        self.visualize_reconstructions(dev, "dev", max_fonts=100)
        self.visualize_reconstructions(dev_hard, "dev_hard", max_fonts=1000)
        self.visualize_reconstructions(test, "test", max_fonts=100)
        self.visualize_reconstructions(test_hard, "test_hard", max_fonts=1000)

    def train_step(self, datum):
        pass

    def evaluate(self, corpus, metric, max_fonts=None):
        if max_fonts is None:
            max_fonts = len(corpus)
        if metric == Metric.nll_mf:
            if self.config.model == enums.Model.font_vae:
                total_nll = 0.0
                for font in corpus[:max_fonts]:
                    datum = font[1]
                    datum = Variable(self.config.float(datum))
                    total_nll -= float(self.forward(datum))
                num_letters = 26. * max_fonts
                return total_nll / num_letters
            else:
                return 0.0
        elif metric == Metric.mse:
            total_mse = 0.0
            for i,font in enumerate(corpus[:max_fonts]):
                filename, datum = font
                datum = Variable(self.config.float(datum))
                mask = self.get_mask(filename)
                datum_hat = self.reconstruct(datum, mask)
                # only compute the loss over the unobserved characters
                if self.config.blanks == 0:
                    mask = torch.zeros_like(mask)
                if filename in self.config.test_dict:
                    print filename, float(torch.nn.functional.mse_loss((datum_hat / 255.) * (1-mask), (datum / 255.) * (1-mask), reduction='sum')) / self.config.blanks
                font_mse = float(torch.nn.functional.mse_loss((datum_hat / 255.) * (1-mask), (datum / 255.) * (1-mask), reduction='sum'))
                outfile = open("{}/{}/{}/mse_e{}_b{}_{}.txt".format(paths['images'], self.config.mode, self.config.model, self.curr_epoch, self.config.blanks, filename), 'w')
                outfile.write(str(font_mse / self.config.blanks))
                outfile.close()
                total_mse += font_mse
            if self.config.blanks != 0:
                return total_mse / (max_fonts * self.config.blanks)
            else:
                return total_mse / (max_fonts * 26)
        elif metric == Metric.ssim:
            total_ssim = 0.0
            for i,font in enumerate(corpus[:max_fonts]):
                filename, datum = font
                datum = Variable(self.config.float(datum))
                mask = self.get_mask(filename)
                datum_hat = self.reconstruct(datum, mask)
                if self.config.blanks == 0:
                    mask = torch.zeros_like(mask)
                for j in range(26):
                    if mask[j,0].item() == 0:
                        total_ssim += float(ssim(datum_hat[j].detach().cpu().numpy() / 255., datum[j].detach().cpu().numpy() / 255., data_range=(datum[j].max()-datum[j].min()).item() / 255.))
            if self.config.blanks != 0:
                return total_ssim / (max_fonts * self.config.blanks)
            else:
                return total_ssim / (max_fonts * 26)
        else:
            sys.exit("Unsupported metric")

    def print_metrics(self, corpora, names, max_fonts=None):
        outstring = "E:{:4d}".format(self.curr_epoch)
        values = []
        for metric in list(Metric):
            for corpus, name in zip(corpora, names):
                outstring += "    " + name
                outstring += " {}:".format(metric)
                outstring += "  {:10.4f}"
                values.append(self.evaluate(corpus, metric, max_fonts=max_fonts))
        print outstring.format(*values)

    def sample_fonts(self):
        pass

    def save(self):
        # save model
        save_path = self.get_save_path()
        save_dict = {'state_dict' : self.state_dict(),
                     'config' : self.config,
                     'curr_epoch' : self.curr_epoch,
                     'hyperparams' : self.hyperparams}
        torch.save(save_dict, save_path)

    def load(self):
        checkpoint_dict = torch.load(self.config.load_path)
        self.load_state_dict(checkpoint_dict['state_dict'], strict=False)
        self.config = checkpoint_dict['config']
        self.curr_epoch = checkpoint_dict['curr_epoch']
        self.hyperparams = checkpoint_dict['hyperparams']

    def get_save_path(self):
        filename = "{}_E-{}.pt".format(self, self.curr_epoch)
        return "{}/{}/{}".format(paths['checkpoints'], self.config.mode, filename)

    def __str__(self):
        string = ""
        for k,v in self.hyperparams.items():
            string += "{}-{}_".format(k, v)
        return string
